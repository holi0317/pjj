package fanbox

import (
	"net/http"
	"net/http/cookiejar"
	"net/url"

	"github.com/sirupsen/logrus"
	"gitlab.com/holi0317/pjj/common"
	"golang.org/x/net/publicsuffix"
)

type Client struct {
	// Token from cookie key FANBOXSESSID
	Token string

	// Cached http client.
	// Use method HTTPClient to lazily initialize a HTTP client.
	httpClient *http.Client
}

// HTTPClient returns a non-nil http client pointer.
//
// If the field `httpClient` is not nil, this will return that field.
// Otherwise, this function will initialize a new http client and store it
// to `httpClient` field. Then return the client.
//
// The return http client will populate headers (user agent, referrer, etc) and
// cookies (authentication token) automatically.
func (c *Client) HTTPClient() *http.Client {
	if c.httpClient != nil {
		return c.httpClient
	}

	rt := common.NewWithHeaderTransport(nil)
	rt.Header.Set("Referer", "https://www.fanbox.cc")
	rt.Header.Set("Origin", "https://www.fanbox.cc")
	rt.Header.Set("User-Agent", common.UserAgent)

	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		logrus.WithError(err).Fatal("Failed to initialize cookiejar")
	}

	c.httpClient = &http.Client{
		Timeout:   common.HTTPTimeout,
		Transport: rt,
		Jar:       jar,
	}

	// FIXME: How to set this cookie on domain `fanbox.cc` and will send in subdomains?
	u, err := url.Parse("https://api.fanbox.cc")
	if err != nil {
		panic(err)
	}

	c.httpClient.Jar.SetCookies(u, []*http.Cookie{
		{Name: "FANBOXSESSID", Value: c.Token, Secure: true, HttpOnly: true},
	})

	u, err = url.Parse("https://downloads.fanbox.cc")
	if err != nil {
		panic(err)
	}
	c.httpClient.Jar.SetCookies(u, []*http.Cookie{
		{Name: "FANBOXSESSID", Value: c.Token, Secure: true, HttpOnly: true},
	})

	return c.httpClient
}
