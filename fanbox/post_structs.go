package fanbox

import (
	"encoding/json"
	"time"

	"github.com/sirupsen/logrus"
)

type PostBody interface {
	ToPostImages(log logrus.FieldLogger) []PostImage
}

type ArticleBody struct {
	Blocks []struct {
		Type    string `json:"type"`
		Text    string `json:"text,omitempty"`
		ImageID string `json:"imageId,omitempty"`
	} `json:"blocks"`
	ImageMap map[string]PostImage `json:"imageMap"`
}

func (body *ArticleBody) ToPostImages(log logrus.FieldLogger) []PostImage {
	res := make([]PostImage, 0)

	for _, block := range body.Blocks {
		if block.Type != "image" {
			continue
		}

		if block.ImageID == "" {
			log.Warn("Got an image without image ID")
			continue
		}

		image, found := body.ImageMap[block.ImageID]
		if !found {
			log.WithField("imageID", block.ImageID).Warn("Got image ID but it does not exist in map")
			continue
		}

		res = append(res, image)
	}

	return res
}

type ImageBody struct {
	Text   string      `json:"text"`
	Images []PostImage `json:"images"`
}

func (body *ImageBody) ToPostImages(log logrus.FieldLogger) []PostImage {
	return body.Images
}

type TextBody struct {
	Text string `json:"text"`
}

func (body *TextBody) ToPostImages(log logrus.FieldLogger) []PostImage {
	return nil
}

type FileBody struct{}

func (body *FileBody) ToPostImages(log logrus.FieldLogger) []PostImage {
	return nil
}

type ListCreatorPostResponse struct {
	Body []struct {
		ID                string    `json:"id"`
		Title             string    `json:"title"`
		PublishedDatetime time.Time `json:"publishedDatetime"`
		UpdatedDatetime   time.Time `json:"updatedDatetime"`
		IsRestricted      bool      `json:"isRestricted"`
		CreatorID         string    `json:"creatorId"`
		HasAdultContent   bool      `json:"hasAdultContent"`
	} `json:"body"`
}

type PostInfoResponse struct {
	Body struct {
		ID                string           `json:"id"`
		Title             string           `json:"title"`
		CoverImageURL     string           `json:"coverImageUrl"`
		FeeRequired       int              `json:"feeRequired"`
		PublishedDatetime time.Time        `json:"publishedDatetime"`
		UpdatedDatetime   time.Time        `json:"updatedDatetime"`
		Type              string           `json:"type"`
		Body              *json.RawMessage `json:"body"`
		Excerpt           string           `json:"excerpt"`
		IsRestricted      bool             `json:"isRestricted"`
		CreatorID         string           `json:"creatorId"`
		HasAdultContent   bool             `json:"hasAdultContent"`
	} `json:"body"`
}
