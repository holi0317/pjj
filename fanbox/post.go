package fanbox

import (
	"context"
	"encoding/json"
	"net/http"
	"net/url"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/holi0317/pjj/common"
)

type paginate struct {
	Body []string `json:"body"`
}

type Post struct {
	ID                string
	Title             string
	CreatorID         string
	PublishedDatetime time.Time
	UpdatedDatetime   time.Time

	// If true, user has no permission to view this post
	IsRestricted bool
	// Images     []PostImage
}

type PostImage struct {
	ID           string `json:"id"` // Adding JSON tags here to reuse them in unmarshal
	Extension    string `json:"extension"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
	OriginalURL  string `json:"originalUrl"`
	ThumbnailURL string `json:"thumbnailUrl"`
}

// ListCreatorPost fetch all posts from the given creator ID
func (c *Client) ListCreatorPost(log logrus.FieldLogger, creatorid string) ([]Post, error) {
	log = log.WithField("creator_id", creatorid)

	log.Debug("Listing creator posts")

	pages, err := c.paginateCreator(log, creatorid)
	if err != nil {
		return nil, err
	}

	allPosts := make([]Post, 0)

	for _, u := range pages.Body {
		posts, err := c.listCreatorPostByURL(log, u)
		if err != nil {
			return nil, err
		}

		allPosts = append(allPosts, posts...)
	}

	log.WithField("posts_len", len(allPosts)).Debug("Listed all creator's posts")

	return allPosts, nil
}

func (c *Client) paginateCreator(log logrus.FieldLogger, creatorid string) (paginate, error) {
	var paginateResp paginate
	log = log.WithField("creator_id", creatorid)

	log.Debug("Listing creator pages")

	u, err := url.Parse("https://api.fanbox.cc/post.paginateCreator")
	if err != nil {
		panic(err)
	}

	q := u.Query()
	q.Add("creatorId", creatorid)
	u.RawQuery = q.Encode()

	req, err := http.NewRequestWithContext(context.TODO(), "GET", "", nil)
	if err != nil {
		panic(err)
	}
	req.URL = u
	client := c.HTTPClient()

	log.Debug("Sending HTTP request")

	resp, err := client.Do(req)
	if err != nil {
		return paginateResp, err
	}
	defer resp.Body.Close()

	log.WithField("status_code", resp.StatusCode).Trace("Got HTTP response")
	err = common.CheckStatus(resp)
	if err != nil {
		return paginateResp, err
	}

	decoder := json.NewDecoder(resp.Body)

	log.Debug("Decoding response body as JSON")

	err = decoder.Decode(&paginateResp)
	if err != nil {
		return paginateResp, err
	}

	return paginateResp, nil
}

// listCreatorPostByURL gets the given URL and try to get posts.
func (c *Client) listCreatorPostByURL(log logrus.FieldLogger, u string) ([]Post, error) {
	log = log.WithField("url", u)

	req, err := http.NewRequestWithContext(context.TODO(), "GET", u, nil)
	if err != nil {
		panic(err)
	}
	client := c.HTTPClient()

	log.Debug("Sending HTTP request")

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	log.WithField("status_code", resp.StatusCode).Trace("Got HTTP response")
	err = common.CheckStatus(resp)
	if err != nil {
		return nil, err
	}

	var postResp ListCreatorPostResponse
	decoder := json.NewDecoder(resp.Body)

	log.Debug("Decoding response body as JSON")

	err = decoder.Decode(&postResp)
	if err != nil {
		return nil, err
	}

	log.Debug("Decoded JSON. Normalizing the fields")

	result := make([]Post, 0)
	for _, item := range postResp.Body {
		post := Post{
			ID:                item.ID,
			Title:             item.Title,
			CreatorID:         item.CreatorID,
			PublishedDatetime: item.PublishedDatetime,
			UpdatedDatetime:   item.UpdatedDatetime,
			IsRestricted:      item.IsRestricted,
		}

		result = append(result, post)
	}

	log.Debug("Request and parse succeed")

	return result, nil
}

// ListImages lists the images found in this post
func (c *Client) ListImages(ctx context.Context, log logrus.FieldLogger, post *Post) ([]PostImage, error) {
	log = log.WithFields(logrus.Fields{
		"post_id":    post.ID,
		"creator_id": post.CreatorID,
	})

	// Build URL
	u, err := url.Parse("https://api.fanbox.cc/post.info")
	if err != nil {
		panic(err)
	}
	q := u.Query()
	q.Add("postId", post.ID)
	u.RawQuery = q.Encode()

	log = log.WithField("url", u)

	// Build request
	req, err := http.NewRequestWithContext(ctx, "GET", "", nil)
	if err != nil {
		panic(err)
	}
	req.URL = u
	client := c.HTTPClient()

	log.Debug("Sending HTTP request")

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	log.WithField("status_code", resp.StatusCode).Trace("Got HTTP response")
	err = common.CheckStatus(resp)
	if err != nil {
		return nil, err
	}

	var postResp PostInfoResponse
	decoder := json.NewDecoder(resp.Body)

	log.Debug("Decoding response body as JSON")

	err = decoder.Decode(&postResp)
	if err != nil {
		return nil, err
	}

	log.Debug("Decoded JSON. Scraping images from the post")

	if postResp.Body.Body == nil {
		log.Warn("Post body is nil. Did you check for permission?")
		return nil, nil
	}

	var body PostBody
	log = log.WithField("post_type", postResp.Body.Type)

	// Unmarshal tagged union JSON
	// Ref: https://stackoverflow.com/a/55995306/13226397
	switch postResp.Body.Type {
	case "image":
		body = new(ImageBody)
	case "article":
		body = new(ArticleBody)
	case "text":
		body = new(TextBody)
	case "file":
		body = new(FileBody)
	default:
		log.Warn("Unknown post type")
		return nil, nil
	}

	err = json.Unmarshal(*postResp.Body.Body, &body)
	if err != nil {
		log.WithError(err).Warn("Failed to parse post body")
		return nil, nil
	}

	return body.ToPostImages(log), nil
}
