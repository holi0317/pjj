build:
	goreleaser build --snapshot --clean --single-target

test:
	go test ./...

lint:
	golangci-lint run -v

clean:
	rm -rf target
