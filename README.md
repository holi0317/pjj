# pjj

Batch download artworks from the glorious P site we all love.

Note that this is (mostly) an one-off script. If this project did not update for a
long time, there's high chance this script will fail to work

# Requirements

- Go
- Make (for compilation)
- golangci-lint
- goreleaser

# Compile

```bash
# Compile for current platform + arch
make build

# Compile for all supported platforms + arch
make buildall
```

This command will output all built artifacts under `dist/` directory.

# Run unit test and linting

```bash
make test
make lint
```

# Usage

Run the binary with `--help` flag to get help.

In general, this tool takes a list of artist ID in configuration file and download
all of their artworks to current directory.

To generate a configuration file, run `init` subcommand.

To download arts, use `download` subcommand.
