package pixiv

import (
	"encoding/json"
	"testing"

	"github.com/sirupsen/logrus"
)

const artJSON string = `{
  "error": false,
  "message": "",
  "body": {
    "illustId": "25565",
    "id": "25565",
    "illustType": 1,
    "restrict": 0,
    "xRestrict": 0,
    "sl": 2,
    "urls": {
      "original": "https://i.pximg.net/img-original/img/2019/09/18/21/30/01/25565_p0.jpg"
    },
    "userId": "65535",
    "userName": "65535user",
    "userAccount": "65565userloginID",
    "pageCount": 3
  }
}
`

func TestArtRoot(t *testing.T) {
	t.Run("Should successfully parse the given JSON", func(t *testing.T) {
		var root artRoot
		err := json.Unmarshal([]byte(artJSON), &root)
		if err != nil {
			t.Errorf("Failed to parse json data %s", err)
		}

		if root.Body.UserID != "65535" {
			t.Errorf("User ID failed. Expected 65535, got %s", root.Body.UserID)
		}

		if root.Body.IllustID != "25565" {
			t.Errorf("Illust ID failed. Expected 25565, got %s", root.Body.IllustID)
		}

		if root.Body.Count != 3 {
			t.Errorf("Count failed. Expected 3, got %d", root.Body.Count)
		}

		if root.Body.URLs.Original != "https://i.pximg.net/img-original/img/2019/09/18/21/30/01/25565_p0.jpg" {
			t.Errorf("URL failed. Got %s", root.Body.URLs.Original)
		}
	})

	t.Run("Should get extension correctly", func(t *testing.T) {
		var root artRoot
		err := json.Unmarshal([]byte(artJSON), &root)
		if err != nil {
			t.Errorf("Failed to parse json data %s", err)
		}

		if ext := root.Ext(); ext != ".jpg" {
			t.Errorf("Expected extension to be '.jpg'. Got %s", ext)
		}
	})

	t.Run("Should get download request for given index correctly", func(t *testing.T) {
		var root artRoot
		err := json.Unmarshal([]byte(artJSON), &root)
		if err != nil {
			t.Errorf("Failed to parse json data %s", err)
		}
		client := &Client{}

		req, err := root.MakeDownload(logrus.New(), client, 1)
		if err != nil {
			t.Errorf("Expected to have no error. Got %s", err)
		}

		if req.Source != "https://i.pximg.net/img-original/img/2019/09/18/21/30/01/25565_p1.jpg" {
			t.Errorf("Unexpected source. %s", req.Source)
		}

		if req.Dest != "65535/25565-01.jpg" {
			t.Errorf("Unexpected dest. %s", req.Dest)
		}
	})
}
