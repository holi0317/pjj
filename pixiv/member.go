package pixiv

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/holi0317/pjj/common"
)

// memberRoot struct contains the structure for response for a member ajax request
// Construct this struct by json.Unmarshal
type memberRoot struct {
	Body map[string]interface{} `json:"body"`
}

// GetArts will get all artwork IDs from the member
// Including both illustrations and manga
// if either illustration or manga is nil, nil slice will be returned
func (root *memberRoot) GetArts() []string {
	body := root.Body

	res := make([]string, 0)

	for _, key := range []string{"illusts", "manga"} {
		val, exist := body[key]
		if !exist {
			continue
		}

		m, valid := val.(map[string]interface{})
		if !valid {
			continue
		}

		for key := range m {
			res = append(res, key)
		}

	}

	return res
}

// fetchMember will go to the server and get all artwork (including illustrations and manga)
// of the member
//
// If the member does not have any artwork, nil slice will be returned
func (c *Client) FetchMember(ctx context.Context, log logrus.FieldLogger, member string) ([]string, error) {
	log = log.WithField("member", member)

	log.Debug("Fetching member artwork")

	url := fmt.Sprintf("https://www.pixiv.net/ajax/user/%s/profile/all", member)
	req, err := http.NewRequestWithContext(ctx, "GET", url, nil)
	if err != nil {
		return nil, err
	}
	client := c.getHTTPClient()

	log.Debug("Sending HTTP request")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	log.WithField("status_code", resp.StatusCode).Trace("Got HTTP response")
	err = common.CheckStatus(resp)
	if err != nil {
		return nil, err
	}

	var root memberRoot
	decoder := json.NewDecoder(resp.Body)

	log.Debug("Decoding response body as JSON")
	err = decoder.Decode(&root)
	if err != nil {
		return nil, err
	}

	log.Debug("Successfully parsed JSON result")

	return root.GetArts(), nil
}
