package pixiv

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"path"

	"github.com/sirupsen/logrus"
	"github.com/spf13/afero"
	"gitlab.com/holi0317/pjj/common"
)

// Path prefix (aka directory) for output
const outputPrefix = "pixiv"

// artRoot is the JSON response for illustration endpoint https://www.pixiv.net/ajax/illust/:id
// Construct this struct with json.Unmarshal
type artRoot struct {
	Body artBody `json:"body"`
}

type artBody struct {
	UserID   string  `json:"userId"`
	IllustID string  `json:"illustId"`
	Count    int     `json:"pageCount"`
	URLs     artURLs `json:"urls"`
}

type artURLs struct {
	Original string `json:"original"`
}

// Ext returns the extension of the artworks
func (root *artRoot) Ext() string {
	return path.Ext(root.Body.URLs.Original)
}

// zfill gets the number of padding for '0' required for destination file
func (root *artRoot) zfill() int {
	length := common.IntLength(root.Body.Count)
	if length < 2 {
		return 2
	}

	return length
}

// MakeDownload will construct a download request for the given index
// of image in the album
func (root *artRoot) MakeDownload(log logrus.FieldLogger, client *Client, index int) (*common.DownloadReq, error) {
	if index < 0 || index >= root.Body.Count {
		return nil, errors.New("Index out of bound")
	}

	ext := root.Ext()
	zfill := root.zfill()

	u, err := url.Parse(root.Body.URLs.Original)
	if err != nil {
		return nil, err
	}

	relPath := fmt.Sprintf("%s_p%d%s", root.Body.IllustID, index, ext)
	u, err = u.Parse(relPath)
	if err != nil {
		return nil, err
	}

	dest := fmt.Sprintf("%s/%s-%0*d%s", root.Body.UserID, root.Body.IllustID, zfill, index, ext)

	fsys := afero.NewBasePathFs(afero.NewOsFs(), outputPrefix)

	return common.NewDownloadReq(u.String(), dest, client.getHTTPClient(), fsys, log), nil
}

// downloadArtwork will download given artwork and save it to disk
//
// If this function return nil, that means either download succeed or
// destination file exist. Probably downloaded in the past
func (c *Client) DownloadArtwork(ctx context.Context, log logrus.FieldLogger, artwork string) error {
	log = log.WithField("artwork", artwork)

	log.Debug("Getting art list")

	url := fmt.Sprintf("https://www.pixiv.net/ajax/illust/%s", artwork)
	req, err := http.NewRequestWithContext(ctx, "GET", url, nil)
	if err != nil {
		return err
	}

	client := c.getHTTPClient()
	log.Debug("Sending HTTP request")
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	log.WithField("status_code", resp.StatusCode).Trace("Got HTTP response")
	err = common.CheckStatus(resp)
	if err != nil {
		return err
	}

	var root artRoot
	decoder := json.NewDecoder(resp.Body)
	log.Debug("Decoding response body as JSON")

	err = decoder.Decode(&root)
	if err != nil {
		return err
	}

	log.WithFields(logrus.Fields{
		"root":  root,
		"count": root.Body.Count,
	}).Debug("Parsed artwork JSON")

	for i := 0; i < root.Body.Count; i++ {
		req, err := root.MakeDownload(log, c, i)
		if err != nil {
			return err
		}

		err = req.Exec(ctx)
		if err != nil {
			return err
		}
	}

	return nil
}
