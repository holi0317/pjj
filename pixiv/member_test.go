package pixiv

import (
	"encoding/json"
	"testing"
)

func TestMemberRoot(t *testing.T) {
	t.Run("Should parse nothing for all empty array", func(t *testing.T) {
		var root memberRoot
		rawJSON := `{
			"error": false,
			"message": "",
			"body": {
				"illusts": [],
				"manga": [],
				"novels": [],
				"mangaSeries": [],
				"novelSeries": [],
				"pickup": []
			}
		}`
		err := json.Unmarshal([]byte(rawJSON), &root)
		if err != nil {
			t.Errorf("Expected to parse JSON successfully. %s", err)
		}

		res := root.GetArts()
		if len(res) != 0 {
			t.Errorf("Expected empty art slice. Got len(res) = %d", len(res))
		}
	})

	t.Run("Should parse id from manga map", func(t *testing.T) {
		var root memberRoot
		rawJSON := `{
			"error": false,
			"message": "",
			"body": {
				"illusts": [],
				"manga": {
					"25565": null
				},
				"novels": [],
				"mangaSeries": [],
				"novelSeries": [],
				"pickup": []
			}
		}`
		err := json.Unmarshal([]byte(rawJSON), &root)
		if err != nil {
			t.Errorf("Expected to parse JSON successfully. %s", err)
		}

		res := root.GetArts()
		if len(res) != 1 {
			t.Errorf("Expected art slice with 1 length. Got len(res) = %d", len(res))
		}

		if res[0] != "25565" {
			t.Errorf("Expected art could extract ID from manga")
		}
	})
}
