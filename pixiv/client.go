package pixiv

import (
	"net/http"
	"net/http/cookiejar"
	"net/url"

	"github.com/sirupsen/logrus"
	"gitlab.com/holi0317/pjj/common"
	"golang.org/x/net/publicsuffix"
)

type Client struct {
	Token string

	// Cached http client.
	// Use method getHttpClient to lazily initialize a HTTP client.
	httpClient *http.Client
}

// getHTTPClient returns a non-nil http client pointer.
//
// If the field `httpClient` is not nil, this will return that field.
// Otherwise, this function will initialize a new http client and store it
// to `httpClient` field. Then return the client.
//
// The return http client will populate headers (user agent, referrer, etc) and
// cookies (authentication token) automatically.
func (c *Client) getHTTPClient() *http.Client {
	if c.httpClient != nil {
		return c.httpClient
	}

	rt := common.NewWithHeaderTransport(nil)
	rt.Header.Set("Referer", "https://www.pixiv.net/")
	rt.Header.Set("User-Agent", common.UserAgent)

	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		logrus.WithError(err).Fatal("Failed to initialize cookiejar")
	}

	c.httpClient = &http.Client{
		Timeout:   common.HTTPTimeout,
		Transport: rt,
		Jar:       jar,
	}

	u, err := url.Parse("https://pixiv.net")
	if err != nil {
		panic(err)
	}

	c.httpClient.Jar.SetCookies(u, []*http.Cookie{
		{Name: "PHPSESSID", Value: c.Token, Secure: true, HttpOnly: true},
	})

	return c.httpClient
}
