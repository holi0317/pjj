package cmd

import (
	"sort"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

func App() *cli.App {
	g := &globalSetting{}

	app := cli.NewApp()
	app.Name = "pjj"
	app.Description = "CLI for batch downloading images from pixiv.net"
	app.Usage = "Download arts from pixiv"

	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:    "loglevel",
			Value:   "INFO",
			Usage:   "Log level of the program",
			EnvVars: []string{"PJJ_LOGLEVEL"},
		},

		&cli.StringFlag{
			Name:    "config",
			Aliases: []string{"c"},
			Value:   "config.toml",
			Usage:   "TOML file for storing the configuration",
		},

		&cli.StringFlag{
			Name:  "pixiv-seen",
			Value: "seen-pixiv.txt",
			Usage: "Text file for storing seen Pixiv arts ID",
		},

		&cli.StringFlag{
			Name:  "fanbox-seen",
			Value: "seen-fanbox.txt",
			Usage: "Text file for storing seen Fanbox post ID",
		},
	}

	app.Commands = []*cli.Command{
		downloadCmd(g),
		initCmd(g),
		scanCmd(g),
		rmcorruptCmd(g),
	}

	app.Before = func(c *cli.Context) error {
		var err error

		// Setup logger level
		lvl := c.String("loglevel")

		if lvl != "" {
			level, err := logrus.ParseLevel(lvl)
			if err != nil {
				logrus.WithField("lvl", lvl).Warn("Got invalid level for PJJ_LOG")
			} else {
				logrus.SetLevel(level)
				logrus.WithField("level", level).Info("Set log level")
			}
		}

		// Setup global object
		cfgpath := c.String("config")
		pSeenpath := c.String("pixiv-seen")
		fbSeenpath := c.String("fanbox-seen")

		err = g.readConfigs(cfgpath, pSeenpath, fbSeenpath)
		if err != nil {
			return err
		}

		logrus.Info("Configuration and seen file read succeed")

		return nil
	}

	app.After = func(c *cli.Context) error {
		err := g.PixivSeen.Write()
		if err != nil {
			return err
		}

		err = g.FanboxSeen.Write()
		if err != nil {
			return err
		}

		return nil
	}

	// Sort flags and commands by their name
	sort.Sort(cli.FlagsByName(app.Flags))
	sort.Sort(cli.CommandsByName(app.Commands))

	return app
}
