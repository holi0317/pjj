package cmd

import (
	"fmt"
	"sync"

	"github.com/gammazero/workerpool"
	"github.com/sirupsen/logrus"
	"github.com/spf13/afero"
	"gitlab.com/holi0317/pjj/common"
	"gitlab.com/holi0317/pjj/fanbox"
	"golang.org/x/net/context"
)

type fanboxDownloader struct {
	g   *globalSetting
	ctx context.Context
	log logrus.FieldLogger
}

func (f *fanboxDownloader) Download() {
	if f.log == nil {
		f.log = logrus.WithField("downloader", "fanbox")
	}

	f.log.WithField("len", len(f.g.Cfg.FanboxUsers)).Info("Collecting fanbox arts to download")

	posts := f.collectPosts()
	f.log.WithField("posts_len", len(posts)).Info("Collected posts to download")

	f.downloadPosts(posts)
	f.log.Info("Fanbox download completed")
}

// collectPosts will fetch all posts created by `f.g.Cfg.FanboxUsers` and
// return posts where the ID does not exist in `f.g.FanboxSeen` slice.
func (f *fanboxDownloader) collectPosts() []fanbox.Post {
	wp := workerpool.New(maxConcurrent)

	res := make([]fanbox.Post, 0)
	resLock := &sync.Mutex{}
	seenMap := f.g.FanboxSeen.AsMap()

	client := f.g.FanboxClient()

	for _, creatorID := range f.g.Cfg.FanboxUsers {
		wp.Submit(func() {
			log := f.log.WithField("creator_id", creatorID)

			posts, err := client.ListCreatorPost(log, creatorID)
			if err != nil {
				log.WithError(err).Warn("Failed to get posts")
				return
			}

			log.Debug("Filtering seen and restricted posts")

			localRes := make([]fanbox.Post, 0)

			for _, post := range posts {
				log := log.WithField("post_id", post.ID)

				_, exist := seenMap[post.ID]
				if exist {
					log.Trace("Post already seen. Skipping")
					continue
				}

				if post.IsRestricted {
					log.Trace("No permission to view post. Skipping")
					continue
				}

				log.Debug("Collecting new post")
				localRes = append(localRes, post)
			}

			log.WithField("res_len", len(localRes)).Info("Collected and filtered posts")

			resLock.Lock()
			defer resLock.Unlock()

			res = append(res, localRes...)
		})
	}

	wp.StopWait()

	return res
}

func (f *fanboxDownloader) downloadPosts(posts []fanbox.Post) {
	fsys := afero.NewBasePathFs(afero.NewOsFs(), "fanbox")
	client := f.g.FanboxClient()

	wp := workerpool.New(maxConcurrent)

	for _, post := range posts {
		wp.Submit(func() {
			log := f.log.WithFields(logrus.Fields{
				"post_id":    post.ID,
				"creator_id": post.CreatorID,
			})

			log.Info("Listing images in the post")

			images, err := client.ListImages(f.ctx, log, &post)
			if err != nil {
				log.WithError(err).Warn("Failed to list post images")
				return
			}

			log.WithField("len", len(images)).Info("Listed images in the post")

			fsys := afero.NewBasePathFs(fsys, post.CreatorID)

			// Number of padding '0' required for filename
			zfill := common.IntLength(len(images))
			if zfill < 2 {
				zfill = 2
			}

			for index, img := range images {
				log := log.WithField("image_index", index)

				filename := fmt.Sprintf("%s-%0*d.%s", post.ID, zfill, index, img.Extension)

				log.Debug("Creating download request for image")

				downloadReq := common.NewDownloadReq(img.OriginalURL, filename, client.HTTPClient(), fsys, log)

				log.Info("Downloading image")

				err := downloadReq.Exec(f.ctx)
				if err != nil {
					log.WithError(err).Warn("Failed to download image in post")
					return
				}

				log.Info("Image downloaded")
			}

			log.Info("Post downloaded")

			// All images in this post have been downloaded successfully
			f.g.FanboxSeen.Lock.Lock()
			defer f.g.FanboxSeen.Lock.Unlock()

			f.g.FanboxSeen.Seen = append(f.g.FanboxSeen.Seen, post.ID)
		})
	}

	wp.StopWait()
}
