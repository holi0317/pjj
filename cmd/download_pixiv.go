package cmd

import (
	"context"
	"sync"

	"github.com/gammazero/workerpool"
	"github.com/sirupsen/logrus"
)

type pixivDownloader struct {
	g   *globalSetting
	log logrus.FieldLogger
	ctx context.Context
}

func (p *pixivDownloader) Download() {
	if p.log == nil {
		p.log = logrus.WithField("downloader", "pixiv")
	}

	p.log.WithField("user_len", len(p.g.Cfg.PixivUsers)).Info("Collecting pixiv arts to download")

	arts := p.resolveArts()
	p.log.WithField("arts_len", len(arts)).Info("Collected arts to download")

	p.downloadArtworks(arts)
	p.log.Info("Pixiv download completed")
}

// resolveArts will fetch all artworks created by cfg.Users and
// return slice of artwork IDs.
//
// Artworks in cfg.Seen will be filtered out and will not exist in resChan
func (p *pixivDownloader) resolveArts() []string {
	wp := workerpool.New(maxConcurrent)

	res := make([]string, 0)
	seenMap := p.g.PixivSeen.AsMap()
	resLock := &sync.Mutex{}

	client := p.g.PixivClient()

	for _, member := range p.g.Cfg.PixivUsers {

		wp.Submit(func() {
			log := p.log.WithField("member", member)

			arts, err := client.FetchMember(p.ctx, log, member)
			if err != nil {
				log.WithError(err).Warn("Failed to fetch member's artwork")
				return
			}

			localRes := make([]string, 0)

			for _, art := range arts {
				log := log.WithField("artwork", art)

				if _, exist := seenMap[art]; exist {
					log.Debug("Artwork already seen. Skipping")
					continue
				}

				log.Debug("Adding artwork for download")
				localRes = append(localRes, art)
			}

			log.WithField("len", len(localRes)).Info("Collected arts for the member")

			resLock.Lock()
			defer resLock.Unlock()

			res = append(res, localRes...)
		})
	}

	wp.StopWait()

	return res
}

// downloadArtworks will spawn workers and download given artworks.
//
// After download has completed and succeed, the related artwork ID will
// be appended to `p.g.cfg.PixivSeen` slice.
func (p *pixivDownloader) downloadArtworks(arts []string) {
	wp := workerpool.New(maxConcurrent)

	client := p.g.PixivClient()

	for _, art := range arts {
		wp.Submit(func() {
			log := p.log.WithField("art", art)

			err := client.DownloadArtwork(p.ctx, log, art)
			if err != nil {
				log.WithError(err).Warn("Failed to download artwork")
				return
			}

			p.g.PixivSeen.Lock.Lock()
			defer p.g.PixivSeen.Lock.Unlock()

			p.g.PixivSeen.Seen = append(p.g.PixivSeen.Seen, art)
		})

	}

	wp.StopWait()
}
