package cmd

import (
	"image"
	"io/fs"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strings"
	"sync"

	"github.com/gammazero/workerpool"
	"github.com/manifoldco/promptui"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

// DetectCorrupt walks from the given root directory and inspect if any images file
// touched is corrupted. If an image is corrupted, the path of the image will be included
// in the return slice.
//
// Currently this supports checking jpeg, png and gif images.
//
// Note that we are not accepting `io/fs.FS` interface as at the time of writing (go 1.16),
// the FS interface does not support deleting file.
func DetectCorrupt(root string) ([]string, error) {
	wp := workerpool.New(runtime.NumCPU())

	res := make([]string, 0)
	resLock := &sync.Mutex{}

	err := filepath.WalkDir(root, func(p string, d fs.DirEntry, err error) error {
		if err != nil {
			return nil
		}

		log := logrus.WithField("path", p)

		if d.IsDir() {
			return nil
		}

		ext := strings.ToLower(path.Ext(p))
		log = log.WithField("ext", ext)

		if ext != ".png" && ext != ".jpg" && ext != ".jpeg" {
			return nil
		}

		log.Debug("Submitting worker job for testing image")
		wp.Submit(func() {
			log.Debug("Testing if image is correctly encoded")
			file, err := os.Open(p)
			if err != nil {
				// TODO: Err handle
				log.WithField("err", err).Warn("Failed to open image file")
				return
			}
			defer file.Close()

			_, _, err = image.Decode(file)
			if err == nil {
				// Successfully decoded image. This is a success path
				log.Debug("Image is correctly encoded")
				return
			}

			log.Warn("Found corrupted image")

			// Image decode failed. Add it to res slice
			resLock.Lock()
			defer resLock.Unlock()
			res = append(res, p)
		})

		return nil
	})

	if err != nil {
		wp.Stop()
		return nil, err
	}

	wp.StopWait()

	return res, nil
}

func rmcorruptCmd(g *globalSetting) *cli.Command {
	return &cli.Command{

		Name:  "rmcorrupt",
		Usage: "Remove corrupt files",
		Description: `It is possible that sometimes we downloaded a corrupted image file.
Running this command will delete images under this directory that cannot be decoded as
jpeg or png.
Note that this will not update the seen list. Run scan subcommand for updating seen
list after deleting invalid images.`,
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    "yes",
				Aliases: []string{"y"},
				Usage:   "If set, no confirmation will be done before deleting file",
			},
		},
		Action: func(c *cli.Context) error {
			logrus.Info("Scanning images")
			corrupts, err := DetectCorrupt(".")
			if err != nil {
				return err
			}

			if len(corrupts) == 0 {
				logrus.Info("No corrupt image detected!")
				return nil
			}

			logrus.WithField("corrupts", corrupts).Info("Detected corrupted images")

			yes := c.Bool("yes")
			if !yes {
				prompt := promptui.Select{
					Label: "Do you want to delete corrupted images listed above?",
					Items: []string{"Yes", "No"},
				}

				idx, _, err := prompt.Run()

				if err != nil {
					return err
				}

				if idx == 1 {
					logrus.Info("Rejected deletion")
					return nil
				}
			}

			logrus.WithField("count", len(corrupts)).Info("Deleting files")
			for _, p := range corrupts {
				err := os.Remove(p)
				if err != nil {
					logrus.WithField("err", err).Error("Failed to delete a corrupted image")
				}
			}

			logrus.Info("Corrupted image deleted. Run subcommand scan for rebuilding the index if needed")

			return nil
		},
	}
}
