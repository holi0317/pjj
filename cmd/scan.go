package cmd

import (
	"io/fs"
	"os"
	"path"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

func scanCmd(g *globalSetting) *cli.Command {
	return &cli.Command{
		Name:  "scan",
		Usage: "Reconstruct pixiv seen file by scanning image files' name under current directory",
		Description: `We use seen file specified in --seen flag to log down downloaded arts ID for
		faster process time. This commands re-construct the seen file by looking for file names in current directory
		and try to extract the art ID by the filename.
		The seen file will be override by this command without any confirmation.

		Note that this only applies to pixiv but not fanbox.`,
		Action: func(c *cli.Context) error {
			logrus.Info("Scanning for images")

			seen := make(map[string]struct{})

			dirFs := os.DirFS("./pixiv")

			err := fs.WalkDir(dirFs, ".", func(p string, d fs.DirEntry, err error) error {
				log := logrus.WithField("path", p)

				if err != nil {
					return nil
				}

				if d.IsDir() {
					return nil
				}

				ext := strings.ToLower(path.Ext(p))
				log = log.WithField("ext", ext)

				if ext != ".png" && ext != ".jpg" && ext != ".jpeg" {
					return nil
				}

				_, filename := path.Split(p)
				filename = strings.TrimSuffix(filename, ext)
				log = log.WithField("filename", filename)

				log.Debug("Extracting art ID")

				if strings.Count(filename, "-") != 1 {
					log.Warn("Malformed filename: Filename should contain exactly one '-' character")
					return nil
				}

				id := strings.Split(filename, "-")[0]
				log = log.WithField("id", id)

				if _, err := strconv.Atoi(id); err != nil {
					log.Warn("Malformed filename: Art ID should be an integer")
					return nil
				}

				log.Info("Adding to seen slice")

				seen[id] = struct{}{}

				return nil
			})

			if err != nil {
				return err
			}

			g.PixivSeen.Seen = make([]string, 0, len(seen))
			for img := range seen {
				g.PixivSeen.Seen = append(g.PixivSeen.Seen, img)
			}

			logrus.Info("Scan completed")

			return nil
		},
	}
}
