package cmd

import (
	"bufio"
	"os"
	"sync"

	"github.com/pelletier/go-toml"
	"github.com/sirupsen/logrus"
	"gitlab.com/holi0317/pjj/fanbox"
	"gitlab.com/holi0317/pjj/pixiv"
)

type config struct {
	// PixivToken is the cookie string for PHPSESSID on pixiv.net. This is optional and could
	// be an empty string
	PixivToken string `toml:"pixiv_token" comment:"Token got from cookie string PHPSESSID. If empty, 18+ art will not be download"`

	// FanboxToken is the cookie string of FANBOXSESSID on fanbox.cc.
	// If this is empty string, some post may not be downloaded.
	FanboxToken string `toml:"fanbox_token" comment:"Token got from cookie string FANBOXSESSID. If empty, some fanbox post may not be downloaded"`

	// PixivUsers is the list of pixiv user ID to watch
	PixivUsers []string `toml:"pixiv_users" comment:"List of user ID on pixiv in string for downloading"`

	// FanboxUsers is the list of fanbox ID to watch
	FanboxUsers []string `toml:"fanbox_users" comment:"List of user ID on pixiv fanbox in string for downloading"`

	// Path to the config file. This field will not be serialized into the
	// toml file.
	Cfgpath string `toml:"-"`
}

// readConfig parses config file and returns the parsed struct
//
// If config file does not exist, default config will be returned
func readConfig(cfgpath string) (*config, error) {
	log := logrus.WithField("cfgpath", cfgpath)

	log.Info("Reading configuration file")

	file, err := os.Open(cfgpath)
	if os.IsNotExist(err) {
		log.Info("Configuration file does not exist. Returning empty config")
		return &config{
			PixivToken: "",
			PixivUsers: make([]string, 0),
			Cfgpath:    cfgpath,
		}, nil
	}
	if err != nil {
		// Error reading file
		return nil, err
	}

	defer file.Close()

	decoder := toml.NewDecoder(file)

	log.Debug("Decoding config file")

	var cfg config
	if err := decoder.Decode(&cfg); err != nil {
		return nil, err
	}

	log.Debug("Config file decode succeed")
	cfg.Cfgpath = cfgpath

	return &cfg, nil
}

// write current config file to path specified on read
func (cfg *config) Write() error {
	log := logrus.WithField("cfgpath", cfg.Cfgpath)

	log.Info("Writing configuration file")

	file, err := os.Create(cfg.Cfgpath)
	if err != nil {
		return err
	}
	defer file.Close()

	encoder := toml.NewEncoder(file).Order(toml.OrderPreserve).ArraysWithOneElementPerLine(true)
	if err := encoder.Encode(cfg); err != nil {
		return err
	}

	log.Info("Saved configuration file")

	return nil
}

type seenSlice struct {
	// Slice of artwork ID that have been downloaded.
	// Acquire the `Lock` when trying to mutate this slice.
	Seen []string

	// Mutex lock for `Seen` slice
	Lock *sync.Mutex

	// File path for the seen file
	Seenpath string
}

func readSeen(seenpath string) (*seenSlice, error) {
	log := logrus.WithField("seenpath", seenpath)
	log.Info("Reading seen file")

	seen := seenSlice{
		Seen:     make([]string, 0),
		Lock:     &sync.Mutex{},
		Seenpath: seenpath,
	}

	file, err := os.Open(seenpath)
	if os.IsNotExist(err) {
		log.Info("Seen file does not exist. Returning empty seen list")
		return &seen, nil
	}
	if err != nil {
		// Error reading file
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		seen.Seen = append(seen.Seen, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	log.Debug("Seen file read succeed")
	return &seen, nil
}

// SeenMap generate a map where key is the seen artwork ID and value is nothing.
//
// This map can be used for detecthing if given artwork ID exist in Seen array or not.
func (s *seenSlice) AsMap() map[string]struct{} {
	seen := make(map[string]struct{})

	s.Lock.Lock()
	defer s.Lock.Unlock()

	for _, artwork := range s.Seen {
		seen[artwork] = struct{}{}
	}

	return seen
}

// Write the seen slice into read path
func (s *seenSlice) Write() error {
	log := logrus.WithField("seenpath", s.Seenpath)
	log.Info("Writing seen file")

	file, err := os.Create(s.Seenpath)
	if err != nil {
		return err
	}
	defer file.Close()

	s.Lock.Lock()
	defer s.Lock.Unlock()

	for _, art := range s.Seen {
		_, err := file.WriteString(art)
		if err != nil {
			return err
		}

		_, err = file.WriteString("\n")
		if err != nil {
			return err
		}
	}

	log.Info("Seen file has been written")

	return nil
}

type globalSetting struct {
	// Config read from user-provided config file
	Cfg config

	// PixivSeen is a slice of artwork ID that have been downloaded
	PixivSeen seenSlice

	// FanboxSeen is a slice of post ID that have been downloaded
	FanboxSeen seenSlice
}

// readConfigs will read configuration file and seen files into the struct being called on
func (g *globalSetting) readConfigs(cfgpath string, pixivSeenpath string, fanboxSeenpath string) error {
	// Read Config
	cfg, err := readConfig(cfgpath)
	if err != nil {
		return err
	}

	g.Cfg = *cfg

	// Read PixivSeen
	pixivSeen, err := readSeen(pixivSeenpath)
	if err != nil {
		return err
	}
	g.PixivSeen = *pixivSeen

	// Read FanboxSeen
	fanboxSeen, err := readSeen(fanboxSeenpath)
	if err != nil {
		return err
	}
	g.FanboxSeen = *fanboxSeen

	return nil
}

func (g *globalSetting) PixivClient() pixiv.Client {
	return pixiv.Client{
		Token: g.Cfg.PixivToken,
	}
}

func (g *globalSetting) FanboxClient() fanbox.Client {
	return fanbox.Client{
		Token: g.Cfg.FanboxToken,
	}
}
