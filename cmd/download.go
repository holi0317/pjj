package cmd

import (
	"sync"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

func downloadCmd(g *globalSetting) *cli.Command {
	return &cli.Command{
		Name:    "download",
		Aliases: []string{"d"},
		Usage:   "Download artworks from given config file to current working directory",
		Action: func(c *cli.Context) error {
			var wg sync.WaitGroup

			wg.Add(1)
			go func() {
				downloader := pixivDownloader{g: g, ctx: c.Context}
				downloader.Download()
				wg.Done()
			}()

			wg.Add(1)
			go func() {
				downloader := fanboxDownloader{g: g, ctx: c.Context}
				downloader.Download()
				wg.Done()
			}()

			wg.Wait()
			logrus.Info("All download completed")

			return nil
		},
	}
}
