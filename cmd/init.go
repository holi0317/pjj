package cmd

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

func initCmd(g *globalSetting) *cli.Command {
	return &cli.Command{
		Name:  "init",
		Usage: "Create new configuration file",
		Description: `This will generate an empty configuration file at path specified in flag --config.
			If the file already exist, this command will refuse to generate one. In that case, either delete the existing file
			or specify an alternative path by flag --config.`,
		Action: func(c *cli.Context) error {
			cfgpath := c.String("config")
			_, err := os.Stat(cfgpath)

			if err == nil {
				// File exist
				log.WithField("cfgpath", cfgpath).Warn("Config file already exist. Refuse to create a new config file.")
				return nil
			}

			// g.Cfg should be empty. Just write it would be fine
			g.Cfg.Cfgpath = cfgpath
			err = g.Cfg.Write()
			if err != nil {
				return err
			}

			return nil
		},
	}
}
