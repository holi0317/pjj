package common

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"path"

	"github.com/sirupsen/logrus"
	"github.com/spf13/afero"
)

// DownloadReq represent a request for downloading
// Source is the source URL. While Dest is the destination in disk
//
// Use [NewDownloadReq] to create this struct
type DownloadReq struct {
	Source string
	Dest   string
	Client *http.Client
	Fs     afero.Fs
	log    logrus.FieldLogger
}

// Create a new download request. Note that creating the request will not start downloading.
// [fs] and [log] are optional.
func NewDownloadReq(source string, dest string, client *http.Client, fs afero.Fs, log logrus.FieldLogger) *DownloadReq {
	if fs == nil {
		fs = afero.NewOsFs()
	}

	if log == nil {
		log = logrus.New()
	}

	log = log.WithFields(logrus.Fields{
		"source": source,
		"dest":   dest,
		"fs":     fs,
	})

	return &DownloadReq{
		Source: source,
		Dest:   dest,
		Client: client,
		Fs:     fs,
		log:    log,
	}
}

// ensureDir will ensure the destination's parent directory exist
// If the directory does not exist, this function will attempt to create it
// If the destination already has something, then this will return an error
func (meta *DownloadReq) ensureDir() error {
	filepath := meta.Dest

	if meta.DestExist() {
		return fmt.Errorf("File %s exists", filepath)
	}

	meta.log.Debug("Ensure parent directories exist exist")

	parents, _ := path.Split(filepath)

	if err := meta.Fs.MkdirAll(parents, 0755); err != nil {
		return err
	}

	return nil
}

// DestExist checks if the file destination already has a file
// If true, the destination has a file. Exec should refuse to run
func (meta *DownloadReq) DestExist() bool {
	filepath := meta.Dest

	meta.log.Debug("Check destination exist")

	_, err := meta.Fs.Stat(filepath)

	return err == nil
}

// Execute the download plan. Download the url to filesystem.
// If download failed, this will try to delete the created file.
//
// If the file already exist, download will be skipped.
func (meta *DownloadReq) Exec(ctx context.Context) error {
	if meta.DestExist() {
		meta.log.Info("Destination file exist. Skipping download")
		return nil
	}

	err := meta.execWithoutCleanup(ctx)
	if err == nil {
		return nil
	}

	// Error occurred during download. Try to cleanup the file
	meta.log.WithError(err).Debug("Error occurred during download. Trying to cleanup failed file")
	if !meta.DestExist() {
		// No file on FS has been created. Pass the error to caller to handle
		return err
	}

	rmErr := meta.Fs.Remove(meta.Dest)
	if rmErr != nil {
		return fmt.Errorf("Error when removing failed file. %w. From %v", rmErr, err)
	}

	return err
}

func (meta *DownloadReq) execWithoutCleanup(ctx context.Context) error {
	meta.log.Info("Downloading file")

	if err := meta.ensureDir(); err != nil {
		return err
	}

	req, err := http.NewRequestWithContext(ctx, "GET", meta.Source, nil)
	if err != nil {
		return err
	}

	meta.log.Debug("Sending HTTP request")
	resp, err := meta.Client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	err = CheckStatus(resp)
	if err != nil {
		return err
	}

	out, err := meta.Fs.Create(meta.Dest)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	meta.log.Info("File downloaded")

	return nil
}
