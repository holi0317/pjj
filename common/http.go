package common

import (
	"fmt"
	"net/http"
)

// http.Transport with default header override
//
// If `rt` field is nil, `http.DefaultTransport` will be used.
// Ref: https://stackoverflow.com/a/51326483/13226397
type WithHeaderTransport struct {
	http.Header
	rt http.RoundTripper
}

func NewWithHeaderTransport(rt http.RoundTripper) WithHeaderTransport {
	if rt == nil {
		rt = http.DefaultTransport
	}

	return WithHeaderTransport{
		Header: make(http.Header),
		rt:     rt,
	}
}

func (h WithHeaderTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	for k, v := range h.Header {
		req.Header[k] = v
	}

	return h.rt.RoundTrip(req)
}

func CheckStatus(resp *http.Response) error {
	if resp.StatusCode >= 400 {
		return fmt.Errorf("Got error http status code %d", resp.StatusCode)
	}

	return nil
}
