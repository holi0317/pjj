package common

import "math"

// IntLength deduces the number of digits of an int
func IntLength(n int) int {
	if n == 0 {
		return 0
	}

	log := math.Log10(float64(n))
	return int(log) + 1
}
