package common

import "time"

// User agent to be used in all request
const UserAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0"

// Default HTTP timeout value to be set
const HTTPTimeout time.Duration = 20 * time.Second
