module gitlab.com/holi0317/pjj

go 1.23

require (
	github.com/gammazero/workerpool v1.1.3
	github.com/manifoldco/promptui v0.9.0
	github.com/pelletier/go-toml v1.9.5
	github.com/sirupsen/logrus v1.9.3
	github.com/spf13/afero v1.11.0
	github.com/urfave/cli/v2 v2.27.5
	golang.org/x/net v0.32.0
)

require (
	github.com/chzyer/readline v1.5.1 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.5 // indirect
	github.com/gammazero/deque v1.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20240521201337-686a1a2994c1 // indirect
	golang.org/x/sys v0.28.0 // indirect
	golang.org/x/text v0.21.0 // indirect
)
