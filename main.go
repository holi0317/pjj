package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/holi0317/pjj/cmd"
)

func main() {
	app := cmd.App()

	err := app.Run(os.Args)
	if err != nil {
		log.WithError(err).Fatal("Failed")
	}
}
